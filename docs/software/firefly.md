# Firefly III

[Firefly III](https://firefly-iii.org/) is a money management app.

## Access

It is available at [http://money.{{ domain }}/](http://money.{{ domain }}/)